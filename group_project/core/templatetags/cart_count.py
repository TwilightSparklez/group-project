# noinspection PyUnresolvedReferences
from core.models import Order
from django import template

register = template.Library()  # for custom template stuff


@register.filter
def cart_item_count(user):
    if user.is_authenticated:
        objs = Order.objects.filter(user=user, ordered=False)
        return objs[0].items.count() if objs.exists() else 0
    else:
        return 0
