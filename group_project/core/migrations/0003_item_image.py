# Generated by Django 3.1.7 on 2021-04-13 15:14

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('core', '0002_auto_20210401_0048'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='image',
            field=models.ImageField(default='', upload_to='item_images/<django.db.models.fields.CharField>/'),
            preserve_default=False,
        ),
    ]
