# Generated by Django 2.2.5 on 2021-03-31 23:48

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='country_origin',
            field=models.CharField(default=None, max_length=50),
        ),
        migrations.AddField(
            model_name='item',
            name='item_description',
            field=models.TextField(default=None, max_length=250),
        ),
        migrations.AddField(
            model_name='item',
            name='item_size',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='item',
            name='item_type',
            field=models.CharField(default=None, max_length=50),
        ),
        migrations.AlterField(
            model_name='item',
            name='price',
            field=models.DecimalField(decimal_places=2, max_digits=6),
        ),
    ]
