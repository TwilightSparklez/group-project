# Generated by Django 3.1.7 on 2021-04-14 01:04

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('core', '0004_auto_20210413_1647'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='discount_price',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=6, null=True),
        ),
    ]
