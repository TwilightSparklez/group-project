# Generated by Django 3.1.7 on 2021-04-13 15:47

# noinspection PyUnresolvedReferences
import core.storage
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('core', '0003_item_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='country_origin',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='item',
            name='image',
            field=models.ImageField(max_length=150, storage=core.storage.OverwriteStorage(),
                                    upload_to=core.models.save_path),
        ),
        migrations.AlterField(
            model_name='item',
            name='item_description',
            field=models.TextField(max_length=250),
        ),
        migrations.AlterField(
            model_name='item',
            name='item_size',
            field=models.IntegerField(),
        ),
        migrations.AlterField(
            model_name='item',
            name='item_type',
            field=models.CharField(max_length=50),
        ),
    ]
