from django.urls import path

from .views import AddDiscountView, CheckoutView, OrderSummaryView, PaymentView, HomeView, ItemDetailView, \
    RequestRefundView, add_to_cart, alcohol_categories_list, remove_from_cart, remove_single_item_from_cart, signup, \
    logout_user

app_name = 'core'
urlpatterns = [
    path('', HomeView.as_view(), name='home-page'),
    path('checkout', CheckoutView.as_view(), name='checkout-page'),
    path('<int:pk>-<str:slug>/', ItemDetailView.as_view(), name='item-details'),
    path('alcohols', alcohol_categories_list),
    path('order-summary/', OrderSummaryView.as_view(), name='order-summary'),
    path('add-to-cart/<int:pk>-<slug>/', add_to_cart, name='add-to-cart'),
    path('add-discount/', AddDiscountView.as_view(), name='add-discount'),
    path('remove-from-cart/<int:pk>-<slug>/', remove_from_cart, name='remove-from-cart'),
    path('remove-item-from-cart/<int:pk>-<slug>/', remove_single_item_from_cart, name='remove-single-item-from-cart'),
    path('payment/<payment_option>/', PaymentView.as_view(), name='payment'),
    path('request-refund/', RequestRefundView.as_view(), name='request-refund'),
    path(r'accounts/signup', signup, name='account-signup'),
    path(r'accounts/logout/', logout_user, name='logout'),
]
