from django.core.files.storage import FileSystemStorage


class OverwriteStorage(FileSystemStorage):
    # allows us to overwrite files where they already exist
    def get_available_name(self, name, max_length=None):
        """
        Deletes a file and returns the name, so it can be reused.
        Args:
            name: name of file to delete
            max_length: Max length, not used.

        Returns:
            name: name of file
        """
        self.delete(name)
        return name
