from django import forms
from django_countries.fields import CountryField

PAYMENT_CHOICES = (
    ('P', 'Placebo'),
)


class CheckoutForm(forms.Form):
    """
    This handles the checkout data - addresses primarily. Payment data is handled separately by PaymentForm.
    Args:
        forms.Form: Passed from views.CheckoutView

    Attributes:
        shipping_address: first line of the address to ship to, required
        shipping_country: country the shipping address is located in, required
        shipping_zip: the zip code of the shipping address, required
        same_billing_address: Whether the shipping address is the same as the billing address, required, bool
        billing_address: first line of the address to bill the order to, required
        billing_country: country the billing address is located in, required
        billing_zip: the zip code of the billing address, required
        set_default_shipping: whether to save as default address, required, bool
        use_default_shipping: Use default address if available, required, bool
        set_default_billing: whether to save as default address, required, bool
        use_default_billing: Use default address if available, required, bool
        payment_option: what to pay with
    """
    shipping_address = forms.CharField(required=True)
    shipping_country = CountryField(blank_label='(select country)').formfield(
        required=True)
    shipping_zip = forms.CharField(required=True)
    same_billing_address = forms.BooleanField(required=True)

    billing_address = forms.CharField(required=False)
    billing_country = CountryField(blank_label='(select country)').formfield(
        required=False)
    billing_zip = forms.CharField(required=False)
    set_default_shipping = forms.BooleanField(required=False)
    use_default_shipping = forms.BooleanField(required=False)
    set_default_billing = forms.BooleanField(required=False)
    use_default_billing = forms.BooleanField(required=False)

    payment_option = forms.ChoiceField(
        widget=forms.RadioSelect, choices=PAYMENT_CHOICES)


class DiscountForm(forms.Form):
    """
        Allows the adding of a discount code to an order.
        Args:
            forms.Form: Passed from views.CheckoutView

        Attributes:
            code: discount code to use

        """
    code = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Promo code',
        'aria-label': 'Recipient\'s username',
        'aria-describedby': 'basic-addon2'
    }))


class RefundForm(forms.Form):
    """
        Used to grant refunds.
        Args:
            forms.Form: Passed from views.RequestRefundView

        Attributes:
            ref_code: reference code, generated separately, required
            message: reason for requesting refund. Optional
            email: email address. Optional

        """
    ref_code = forms.CharField(required=True)
    message = forms.CharField(widget=forms.Textarea(attrs={
        'rows': 4
    }))
    email = forms.EmailField()


class PaymentForm(forms.Form):
    """
        Used to process payments.
        Args:
            forms.Form: Passed from views.PaymentView

        Attributes:
            card_num: card number to pay with. Required. Length of 16.

        """
    card_num = forms.CharField(max_length=16, required=True, min_length=16)
