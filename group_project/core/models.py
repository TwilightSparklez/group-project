import datetime

from django.conf import settings
from django.db import models
from django.urls import reverse
from django.utils.text import slugify
from django_countries.fields import CountryField
from django_resized import ResizedImageField
from mptt.models import MPTTModel, TreeForeignKey

from .storage import OverwriteStorage

# import datetime

# Create your models here.
ADDRESS_TYPES = (('B', 'Billing'), ('S', 'Shipping'))


# noinspection PyUnusedLocal
def save_path(item, filename):
    """
    Allows us to access item_type, as we cannot do it in the class itself. Creates a filepath to save the image to.
    Args:
        item: item class obj
        filename: not needed

    Returns:
        str: filepath
    """
    # can't access item_type directly, so need function to do so. This creates the filepath to save to
    return f'item_images/{item.item_type}/{item.item_name}'


class Category(MPTTModel):
    """
        Allows us to use a tree structure to create a hierarchy for our alcohols, e.g. bombay sapphire is a gin,
        which is a subcategory of spirit
        Args:
            MPTTModel: --

        Attributes:
            name: name of category, must be unique and =<50 characters
            parent: parent category, e.g. spirit. Can be null and blank (to allow for first level stuff, e.g. spirit)
            slug: path to use as url


        """
    # this allows us to use a tree structure to define what bit an item belongs to, e.g. gin, which is a subcategory
    # of spirit
    name = models.CharField(max_length=50, unique=True)
    parent = TreeForeignKey('self', on_delete=models.CASCADE,
                            null=True, blank=True, related_name='children')
    slug = models.SlugField(
        editable=False,
        max_length=50, unique=True
    )

    class MPTTMeta:
        order_insertion_by = ['name']

    class Meta:
        verbose_name_plural = 'Categories'

    def __str__(self) -> str:
        return self.name

    def get_absolute_url(self):
        """
            Gets the absolute url for the category. Contains all items within that category.
        Returns:

        """
        return reverse('items-by-category', args=[str(self.slug)])

    def save(self, *args, **kwargs):
        """
            Used to save the category's values. Useful when backdating categories.
        Args:
            *args:
            **kwargs:
        """
        value = self.name
        self.slug = slugify(value, allow_unicode=True)
        super().save(*args, **kwargs)


class Item(models.Model):
    """
        Contains each item's data. An item = a product, e.g. bombay sapphire gin.
        Args:
            models.Model: --

        Attributes:
            item_name: name of alcohol, max length =<50 characters
            item_description: short description, max length =<250 characters
            country_origin: country of origin, max length =<50 characters
            item_type: type of alcohol, max length =<50 characters
            category: alcohol category
            item_size: size of alcohol, in centiliters.
            price: price of alcohol, max 6 digits, 2 decimal places
            discount_price: discounted price, blank if not discounted. Same rules as 'price'
            image: Image for the alcohol. Settings for image are stored in 'settings'
            slug: path to use as url


        """
    item_name = models.CharField(max_length=50)
    item_description = models.TextField(max_length=250)
    country_origin = models.CharField(max_length=50)
    item_type = models.CharField(max_length=50)
    category = TreeForeignKey('Category', on_delete=models.CASCADE, null=True, blank=True)
    # in cl
    item_size = models.IntegerField()
    price = models.DecimalField(max_digits=6, decimal_places=2)
    discount_price = models.DecimalField(
        max_digits=6, decimal_places=2, blank=True, null=True)
    image = ResizedImageField(upload_to=save_path, storage=OverwriteStorage(), null=True)
    slug = models.SlugField(
        editable=False,
        max_length=50, unique=True
    )

    def get_add_to_cart_url(self):
        """
            Creates the url to add the item to cart
        Returns:

        """
        return reverse("core:add-to-cart", kwargs={
            'pk': self.id,
            'slug': self.slug
        })

    def get_remove_from_cart_url(self):
        """
            Creates the url to remove the item from cart
        Returns:

        """
        return reverse("core:remove-from-cart", kwargs={
            'pk': self.id,
            'slug': self.slug
        })

    def get_absolute_url(self):
        """
            Creates the url for the item's product page
        Returns:

        """
        kwargs = {
            'pk': self.id,
            'slug': self.slug
        }
        return reverse('core:item-details', kwargs=kwargs)

    def save(self, *args, **kwargs):
        value = self.item_name
        self.slug = slugify(value, allow_unicode=True)
        super().save(*args, **kwargs)

    def __str__(self) -> str:
        return self.item_name


class OrderItem(models.Model):
    """
        Acts as the link between Item and Order. Each Item can have many OrderItems, and each Order is made up of many OrderItems
        Args:
            models.Model: --

        Attributes:
            user: foreign key user identifier, handled by the built in auth system
            ordered: whether order has been placed, bool
            quantity: quantity of the item ordered
            item: item obj

        """
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    ordered = models.BooleanField(default=False)  # has it already been ordered?
    quantity = models.IntegerField(default=1)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)

    def get_total_item_price(self):
        """
            Total price, taking into account price and quantity ordered
        Returns:
            Decimal: quantity*price
        """
        return self.quantity * self.item.price

    def get_total_discount_item_price(self):
        """
            Total price, taking into account discounted price and quantity ordered
        Returns:
            Decimal: quantity* discounted price
        """
        return self.quantity * self.item.discount_price

    def get_amount_saved(self):
        """
            Get the amount saved by minusing the discounted price from the total price.
        Returns:
            Decimal: total price - discounted total price
        """
        return self.get_total_item_price() - self.get_total_discount_item_price()

    def get_final_price(self):
        """
            Get the items price. If it's discounted, then that, otherwise the normal price.
        Returns:
            Decimal:
        """
        return self.get_total_discount_item_price() if self.item.discount_price else self.get_total_item_price()

    def __str__(self) -> str:
        return f'{self.quantity} of {self.item.item_name} available'


class Order(models.Model):
    """
        The overarching Order. Each user can have multiple orders, and each order has multiple OrderItems.
        Args:
            models.Model: --

        Attributes:
            user: foreign key user identifier, handled by the built in auth system
            items: many to many field on OrderItem, one order can have any items.
            start_date: date the order was first initialised (first item added to cart), automatically added
            ordered_date: datetime, filled when ordered=True.
            ordered: bool, whether the order has been ordered and paid for.
            shipping_address: foreign key link to the shipping address held in 'Address'
            billing_address: foreign key link to the billing address held in 'Address'
            payment: foreign key link to the payment used for the order
            discount_code: foreign key link to the discount code (if there is one, null if not) of the order
            being_delivered: bool, wheter its being delivered. If True then delivery_date is automatically set.
            delivery_date: ordered_date + 3 days, date the delivery is planned for
            received: bool, whether the order has been delivered, default False
            refund_requested: bool, whether a refund has bene requested, default False
            refund_granted: bool, whether a refund has bene granted, default False
            ref_code: text reference code for order, automatically set, max length 20
        """
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE)
    items = models.ManyToManyField(OrderItem)

    start_date = models.DateTimeField(auto_now_add=True)
    ordered_date = models.DateTimeField(blank=True, null=True)
    ordered = models.BooleanField(default=False)
    shipping_address = models.ForeignKey(
        'Address', related_name='shipping_address', on_delete=models.SET_NULL, blank=True, null=True)
    billing_address = models.ForeignKey(
        'Address', related_name='billing_address', on_delete=models.SET_NULL, blank=True, null=True)
    payment = models.ForeignKey(
        'Payment', on_delete=models.SET_NULL, blank=True, null=True)
    discount_code = models.ForeignKey(
        'DiscountCode', on_delete=models.SET_NULL, blank=True, null=True)
    being_delivered = models.BooleanField(default=False)

    delivery_date = models.DateTimeField(blank=True, null=True)

    received = models.BooleanField(default=False)
    refund_requested = models.BooleanField(default=False)
    refund_granted = models.BooleanField(default=False)
    ref_code = models.CharField(max_length=20)

    def __str__(self) -> str:
        return self.user.username

    def get_total(self):
        """
        Calculates the total cost if a discount code is used
        Returns:
            float: total cost of order, minus discounted amount
        """
        total = float(sum([i.get_final_price() for i in self.items.all()]))
        if self.discount_code:
            total = total * (abs(float(self.discount_code.percentage) - 100) / 100)
        return total

    def save(self, *args, **kwargs):
        """
        Whenever the order is saved:
        if the ordered file is True and ordered_date is blank, set ordered date as the current date and time
        Then if delivery_date isn't set and ordered date is, set delivery date as ordered_date + 3 days
        Args:
            *args:
            **kwargs:
        """
        if self.ordered and self.ordered_date is None:
            self.ordered_date = datetime.datetime.now()
        if self.delivery_date is None and self.ordered_date is not None:
            self.delivery_date = self.ordered_date.date() + datetime.timedelta(days=3)
        super(Order, self).save(*args, **kwargs)


class Address(models.Model):
    """
        Addresses for each user. Each user can have multiple addresses, as they are set for each Order, unless default=True is used.
        Args:
            models.Model: --

        Attributes:
            user: foreign key user identifier, handled by the built in auth system
            address: first line of a address, max length=100
            country: country the address is located within. Select from list.
            zip: Zipcode of the address, max length 8
            address_type: multiple choice
            default: whether its the default address for the user, default False
        """
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE)
    address = models.CharField(max_length=100)

    country = CountryField(multiple=False)
    zip = models.CharField(max_length=8)
    address_type = models.CharField(max_length=1, choices=ADDRESS_TYPES
                                    )
    default = models.BooleanField(default=False)

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name_plural = 'Addresses'


class Payment(models.Model):
    """
        Payments for each user. Each user can have multiple Payments, as they are set for each Order.
        Args:
            models.Model: --

        Attributes:
            user: foreign key user identifier, handled by the built in auth system
            payment_id: automatically generated payment reference number
            amount: total charged amount
            timestamp: automatically added timestamp of the payment time
            card_number: debit card number, max length 16

        """
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True)
    payment_id = models.CharField(max_length=20)
    amount = models.FloatField()
    timestamp = models.DateTimeField(auto_now_add=True)
    card_number = models.CharField(max_length=16)

    def __str__(self):
        return self.user.username


class DiscountCode(models.Model):
    """
        Discount codes. Each discount code can be used on multiple orders.
        Args:
            models.Model: --

        Attributes:
            code: discount code, max length 30
            percentage: how much of a discount the code gives

        """
    code = models.CharField(max_length=30)
    percentage = models.IntegerField()

    def __str__(self):
        return self.code


class Refund(models.Model):
    """
        Addresses for each user. Each user can have multiple addresses, as they are set for each Order, unless default=True is used.
        Args:
            models.Model: --

        Attributes:
            order: foreign key on Order, links the refund to an order
            reason: why they want the refund
            accepted: bool, whether the refund was granted
            email: email address of the user
        """
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    reason = models.TextField()
    accepted = models.BooleanField(default=False)
    email = models.EmailField()

    def __str__(self):
        return f"{self.pk}"
