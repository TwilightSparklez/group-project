import random
import string

from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ObjectDoesNotExist
from django.http.response import HttpResponseRedirect
from django.shortcuts import redirect
from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from django.views.generic import ListView, DetailView, View

from .forms import CheckoutForm, DiscountForm, RefundForm, PaymentForm
from .models import Item, OrderItem, Order, Address, Payment, DiscountCode, Refund, Category


# Create your views here.

def create_ref_code():
    """
    Generates a random string
    Returns:
        str: 20 character length string made up of lower case ASCII and numbers.
    """
    return ''.join(random.choices(string.ascii_lowercase + string.digits, k=20))


def alcohol_categories_list(request):
    return render(request, 'alcohol_tree.html'), {'alcohols': Category.objects.all()}


def valid_form(values):
    return all(values)  # if any are falsy return false, otherwise true


def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('/')
    else:
        form = UserCreationForm()
    return render(request, 'registration/signup.html', {'form': form})


def logout_user(request):
    logout(request)
    return redirect('/')


@login_required
def add_to_cart(request, pk, slug):
    """
    Adds an item to cart. Login required.
    Args:
        request: request
        pk: item primary key
        slug: item slug

    Returns:
        None: redirects to previous page and adds to cart
    """
    item = get_object_or_404(Item, id=pk, slug=slug)
    order_item, created = OrderItem.objects.get_or_create(
        item=item,
        user=request.user,
        ordered=False
    )
    order_qs = Order.objects.filter(user=request.user, ordered=False)
    if order_qs.exists():
        order = order_qs[0]
        # check if the order item is in the order
        if order.items.filter(item__slug=item.slug).exists():
            order_item.quantity += 1
            order_item.save()
            messages.info(request, "Item quantity updated")
            return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
        else:
            order.items.add(order_item)
            messages.info(request, "Item added to cart")
            return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
    else:
        ordered_date = timezone.now()
        order = Order.objects.create(
            user=request.user, ordered_date=ordered_date)
        order.items.add(order_item)
        messages.info(request, "Item added to cart")
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


@login_required
def remove_from_cart(request, pk, slug):
    """
    Removes an item from cart. Login required.
    Args:
        request: request
        pk: item primary key
        slug: item slug

    Returns:
        None: redirects to previous page and removes item from cart
    """
    item = get_object_or_404(Item, id=pk, slug=slug)
    order_qs = Order.objects.filter(
        user=request.user,
        ordered=False
    )
    if order_qs.exists():
        order = order_qs[0]
        # check if the order item is in the order
        if order.items.filter(item__slug=item.slug).exists():
            order_item = OrderItem.objects.filter(
                item=item,
                user=request.user,
                ordered=False
            )[0]
            order.items.remove(order_item)
            order_item.delete()
            messages.info(request, "Item removed from cart")
            return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
        else:
            messages.info(request, "Item not in cart")
            return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
    else:
        messages.info(request, "No active order")
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


@login_required
def remove_single_item_from_cart(request, pk, slug):
    """
    Reduces the quantity of an item in cart by 1, otherwise removes it entirely
    Args:
        request: request
        pk: item primary key
        slug: item slug

    Returns:
        None: redirects to previous page with quantity changed
    """
    item = get_object_or_404(Item, id=pk, slug=slug)
    order_qs = Order.objects.filter(
        user=request.user,
        ordered=False
    )
    if order_qs.exists():
        order = order_qs[0]
        # check if the order item is in the order
        if order.items.filter(item__slug=item.slug).exists():
            order_item = OrderItem.objects.filter(
                item=item,
                user=request.user,
                ordered=False
            )[0]
            if order_item.quantity > 1:
                order_item.quantity -= 1
                order_item.save()
            else:
                order.items.remove(order_item)
            messages.info(request, "Item quantity updated")
            return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
        else:
            messages.info(request, "Item not in order")
            return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
    else:
        messages.info(request, "No active order")
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


def get_discount_code(request, code):
    """
    Tries to add a discount code, if it works, then it returns the discount code object, otherwise ObjectDoesNotExist Error
    Args:
        request: request
        code: code to try

    Returns:

    """
    try:
        discount_code = DiscountCode.objects.get(code=code)
        return discount_code
    except ObjectDoesNotExist:
        messages.info(request, "Discount code invalid")
        return ObjectDoesNotExist


class HomeView(ListView):
    model = Item
    paginate_by = 24
    template_name = "home-page.html"


class ItemDetailView(DetailView):
    model = Item
    template_name = "product-page.html"


# noinspection SpellCheckingInspection
class CheckoutView(View):
    def get(self, *args, **kwargs):
        """
            Loads the page and any user data, if successful then utilises the post function
        Args:
            *args:
            **kwargs:

        Returns:

        """
        try:
            try:
                order = Order.objects.get(user=self.request.user, ordered=False)  # its not ordered yet
            except TypeError:
                messages.info(
                    self.request, "User not logged in")
                return redirect("core:home-page")
            form = CheckoutForm()  # initialise our form
            context = {
                'form': form,
                'discountform': DiscountForm(),
                'order': order,
                'DISPLAY_COUPON_FORM': True
            }

            shipping_address_qs = Address.objects.filter(
                user=self.request.user,
                address_type='S',
                default=True
            )
            if shipping_address_qs.exists():
                context.update(
                    {'default_shipping_address': shipping_address_qs[0]})

            billing_address_qs = Address.objects.filter(
                user=self.request.user,
                address_type='B',
                default=True
            )
            if billing_address_qs.exists():
                context.update(
                    {'default_billing_address': billing_address_qs[0]})
            return render(self.request, "checkout-page.html", context)
        except ObjectDoesNotExist:
            messages.info(
                self.request, "No active orders for this user profile")
            return redirect("core:home-page")

    def post(self, *args, **kwargs):
        """
            Loads the checkout form and processes the data gathered in the get above.
        Args:
            *args:
            **kwargs:

        Returns:

        """
        form = CheckoutForm(self.request.POST or None)
        try:
            order = Order.objects.get(user=self.request.user, ordered=False)
            if form.is_valid():

                use_default_shipping = form.cleaned_data.get(
                    'use_default_shipping')
                if use_default_shipping:
                    print("Using default shipping address")
                    address_qs = Address.objects.filter(
                        user=self.request.user,
                        address_type='S',
                        default=True
                    )
                    if address_qs.exists():
                        shipping_address = address_qs[0]
                        order.shipping_address = shipping_address
                        order.save()
                    else:
                        messages.info(
                            self.request, "No default shipping address available")
                        return redirect('core:checkout')
                else:
                    print("User is entering a new shipping address")
                    shipping_address = form.cleaned_data.get(
                        'shipping_address')
                    shipping_country = form.cleaned_data.get(
                        'shipping_country')
                    shipping_zip = form.cleaned_data.get('shipping_zip')

                    if valid_form([shipping_address, shipping_country, shipping_zip]):
                        shipping_address = Address(
                            user=self.request.user,
                            address=shipping_address,
                            country=shipping_country,
                            zip=shipping_zip,
                            address_type='S'
                        )
                        shipping_address.save()

                        order.shipping_address = shipping_address
                        order.save()

                        set_default_shipping = form.cleaned_data.get(
                            'set_default_shipping')
                        if set_default_shipping:
                            shipping_address.default = True
                            shipping_address.save()

                    else:
                        messages.info(
                            self.request, "Please fill in the required shipping address fields")

                use_default_billing = form.cleaned_data.get(
                    'use_default_billing')
                same_billing_address = form.cleaned_data.get(
                    'same_billing_address')

                if same_billing_address:
                    billing_address = shipping_address
                    billing_address.pk = None
                    billing_address.save()
                    billing_address.address_type = 'B'
                    billing_address.save()
                    order.billing_address = billing_address
                    order.save()

                elif use_default_billing:
                    print("Using the default billing address")
                    address_qs = Address.objects.filter(
                        user=self.request.user,
                        address_type='B',
                        default=True
                    )
                    if address_qs.exists():
                        billing_address = address_qs[0]
                        order.billing_address = billing_address
                        order.save()
                    else:
                        messages.info(
                            self.request, "No default billing address available")
                        return redirect('core:checkout')
                else:
                    print("User is entering a new billing address")
                    billing_address = form.cleaned_data.get(
                        'billing_address')
                    billing_country = form.cleaned_data.get(
                        'billing_country')
                    billing_zip = form.cleaned_data.get('billing_zip')

                    if valid_form([billing_address, billing_country, billing_zip]):
                        billing_address = Address(
                            user=self.request.user,
                            country=billing_country,
                            zip=billing_zip,
                            address_type='B'
                        )
                        billing_address.save()

                        order.billing_address = billing_address
                        order.save()

                        set_default_billing = form.cleaned_data.get(
                            'set_default_billing')
                        if set_default_billing:
                            billing_address.default = True
                            billing_address.save()

                    else:
                        messages.info(
                            self.request, "Please fill in the required billing address fields")

                payment_option = form.cleaned_data.get('payment_option')

                if payment_option == 'P':
                    return redirect('core:payment', payment_option='Placebo')
                else:
                    messages.warning(
                        self.request, "Invalid payment option selected")
                    return redirect('core:checkout-page')
            else:
                messages.warning(
                    self.request, "Invalid checkout")
                return redirect('core:checkout-page')
        except ObjectDoesNotExist:
            messages.warning(self.request, "You do not have an active order")
            return redirect("core:order-summary")


class PaymentView(View):
    def get(self, *args, **kwargs):
        """
        Loads the page and any user data, if successful then utilises the post function
        Args:
            *args:
            **kwargs:

        Returns:

        """
        order = Order.objects.get(user=self.request.user, ordered=False)
        if order.billing_address:
            context = {
                'order': order,
                'DISPLAY_COUPON_FORM': False,
            }
            return render(self.request, "payment.html", context)
        else:
            messages.warning(
                self.request, "You have not added a billing address")
            return redirect("core:checkout-page")

    def post(self, *args, **kwargs):
        """
            gets the user's order and loads the payment form. If both are valid adds them to the database.
        Args:
            *args:
            **kwargs:

        Returns:

        """
        order = Order.objects.get(user=self.request.user, ordered=False)
        form = PaymentForm(self.request.POST)
        if form.is_valid():
            card_number = form.cleaned_data.get('card_num')

            # create the payment
            payment = Payment()
            payment.user = self.request.user
            payment.payment_id = create_ref_code()
            payment.amount = order.get_total()
            payment.card_number = card_number
            payment.save()

            # assign the payment to the order

            order_items = order.items.all()
            order_items.update(ordered=True)
            for item in order_items:
                item.save()

            order.ordered = True
            order.payment = payment
            order.ref_code = create_ref_code()
            order.save()

            messages.success(self.request, "Your order was successful!")
            return redirect("/")
        else:
            messages.warning(self.request, "Invalid data received")
            return redirect('core:payment', payment_option='Placebo')


class AddDiscountView(View):
    def post(self, *args, **kwargs):
        """
            Adds a discount code if valid, otherwise returns an error
        Args:
            *args:
            **kwargs:

        Returns:

        """
        form = DiscountForm(self.request.POST or None)
        if form.is_valid():
            try:
                code = form.cleaned_data.get('code')
                order = Order.objects.get(
                    user=self.request.user, ordered=False)
                discount = get_discount_code(self.request, code)
                if discount != ObjectDoesNotExist:
                    order.discount_code = discount
                    order.save()
                    messages.success(self.request, "Successfully added discount")
                    return redirect("core:checkout-page")
                else:
                    return redirect("core:checkout-page")

            except ObjectDoesNotExist:
                messages.info(self.request, "No active order")
                return redirect("core:checkout-page")


class RequestRefundView(View):
    def get(self, *args, **kwargs):
        """
            Renders the request_refund page.
        Args:
            *args:
            **kwargs:

        Returns:

        """
        form = RefundForm()
        context = {
            'form': form
        }
        return render(self.request, "request_refund.html", context)

    def post(self, *args, **kwargs):
        """
            Takes the posted data from get and processes it. If valid then the order's refund is granted.
        Args:
            *args:
            **kwargs:

        Returns:

        """
        form = RefundForm(self.request.POST)
        if form.is_valid():
            ref_code = form.cleaned_data.get('ref_code')
            message = form.cleaned_data.get('message')
            email = form.cleaned_data.get('email')
            # edit the order
            try:
                order = Order.objects.get(ref_code=ref_code)
                order.refund_requested = True
                order.save()

                # store the refund
                refund = Refund()
                refund.order = order
                refund.reason = message
                refund.email = email
                refund.save()

                messages.info(self.request, "Your request was received.")
                return redirect("core:request-refund")

            except ObjectDoesNotExist:
                messages.info(self.request, "This order does not exist.")
                return redirect("core:request-refund")


class OrderSummaryView(LoginRequiredMixin, View):
    def get(self, *args, **kwargs):
        """
            Gets the order and displays the items within it, if not ordered.
        Args:
            *args:
            **kwargs:

        Returns:

        """
        try:
            order = Order.objects.get(user=self.request.user, ordered=False)
            context = {
                'object': order
            }
            return render(self.request, 'order_summary.html', context)
        except ObjectDoesNotExist:
            messages.warning(self.request, "No active order")
            return redirect("/")
