# Views

> Auto-generated documentation for [core.views](..\..\core\views.py) module.

- [Group_project](..\README.md#group_project-index) / [Modules](..\MODULES.md#group_project-modules) / [Core](index.md#core) / Views
    - [AddDiscountView](#adddiscountview)
        - [AddDiscountView().post](#adddiscountviewpost)
    - [CheckoutView](#checkoutview)
        - [CheckoutView().get](#checkoutviewget)
        - [CheckoutView().post](#checkoutviewpost)
    - [HomeView](#homeview)
    - [ItemDetailView](#itemdetailview)
    - [OrderSummaryView](#ordersummaryview)
        - [OrderSummaryView().get](#ordersummaryviewget)
    - [PaymentView](#paymentview)
        - [PaymentView().get](#paymentviewget)
        - [PaymentView().post](#paymentviewpost)
    - [RequestRefundView](#requestrefundview)
        - [RequestRefundView().get](#requestrefundviewget)
        - [RequestRefundView().post](#requestrefundviewpost)
    - [add_to_cart](#add_to_cart)
    - [alcohol_categories_list](#alcohol_categories_list)
    - [create_ref_code](#create_ref_code)
    - [get_discount_code](#get_discount_code)
    - [logout_user](#logout_user)
    - [remove_from_cart](#remove_from_cart)
    - [remove_single_item_from_cart](#remove_single_item_from_cart)
    - [signup](#signup)
    - [valid_form](#valid_form)

## AddDiscountView

[[find in source code]](..\..\core\views.py#L461)

```python
class AddDiscountView(View):
```

### AddDiscountView().post

[[find in source code]](..\..\core\views.py#L462)

```python
def post(*args, **kwargs):
```

Adds a discount code if valid, otherwise returns an error

#### Arguments

*args:
**kwargs:

## CheckoutView

[[find in source code]](..\..\core\views.py#L208)

```python
class CheckoutView(View):
```

### CheckoutView().get

[[find in source code]](..\..\core\views.py#L209)

```python
def get(*args, **kwargs):
```

Loads the page and any user data, if successful then utilises the post function

#### Arguments

*args:
**kwargs:

### CheckoutView().post

[[find in source code]](..\..\core\views.py#L257)

```python
def post(*args, **kwargs):
```

Loads the checkout form and processes the data gathered in the get above.

#### Arguments

*args:
**kwargs:

## HomeView

[[find in source code]](..\..\core\views.py#L196)

```python
class HomeView(ListView):
```

## ItemDetailView

[[find in source code]](..\..\core\views.py#L202)

```python
class ItemDetailView(DetailView):
```

## OrderSummaryView

[[find in source code]](..\..\core\views.py#L545)

```python
class OrderSummaryView(LoginRequiredMixin, View):
```

### OrderSummaryView().get

[[find in source code]](..\..\core\views.py#L546)

```python
def get(*args, **kwargs):
```

Gets the order and displays the items within it, if not ordered.

#### Arguments

*args:
**kwargs:

## PaymentView

[[find in source code]](..\..\core\views.py#L396)

```python
class PaymentView(View):
```

### PaymentView().get

[[find in source code]](..\..\core\views.py#L397)

```python
def get(*args, **kwargs):
```

Loads the page and any user data, if successful then utilises the post function

#### Arguments

*args:
**kwargs:

### PaymentView().post

[[find in source code]](..\..\core\views.py#L419)

```python
def post(*args, **kwargs):
```

gets the user's order and loads the payment form. If both are valid adds them to the database.

#### Arguments

*args:
**kwargs:

## RequestRefundView

[[find in source code]](..\..\core\views.py#L492)

```python
class RequestRefundView(View):
```

### RequestRefundView().get

[[find in source code]](..\..\core\views.py#L493)

```python
def get(*args, **kwargs):
```

Renders the request_refund page.

#### Arguments

*args:
**kwargs:

### RequestRefundView().post

[[find in source code]](..\..\core\views.py#L509)

```python
def post(*args, **kwargs):
```

Takes the posted data from get and processes it. If valid then the order's refund is granted.

#### Arguments

*args:
**kwargs:

## add_to_cart

[[find in source code]](..\..\core\views.py#L59)

```python
@login_required
def add_to_cart(request, pk, slug):
```

Adds an item to cart. Login required.

#### Arguments

- `request` - request
- `pk` - item primary key
- `slug` - item slug

#### Returns

- `None` - redirects to previous page and adds to cart

## alcohol_categories_list

[[find in source code]](..\..\core\views.py#L31)

```python
def alcohol_categories_list(request):
```

## create_ref_code

[[find in source code]](..\..\core\views.py#L22)

```python
def create_ref_code():
```

Generates a random string

#### Returns

- `str` - 20 character length string made up of lower case ASCII and numbers.

## get_discount_code

[[find in source code]](..\..\core\views.py#L178)

```python
def get_discount_code(request, code):
```

Tries to add a discount code, if it works, then it returns the discount code object, otherwise ObjectDoesNotExist Error

#### Arguments

- `request` - request
- `code` - code to try

## logout_user

[[find in source code]](..\..\core\views.py#L54)

```python
def logout_user(request):
```

## remove_from_cart

[[find in source code]](..\..\core\views.py#L99)

```python
@login_required
def remove_from_cart(request, pk, slug):
```

Removes an item from cart. Login required.

#### Arguments

- `request` - request
- `pk` - item primary key
- `slug` - item slug

#### Returns

- `None` - redirects to previous page and removes item from cart

## remove_single_item_from_cart

[[find in source code]](..\..\core\views.py#L137)

```python
@login_required
def remove_single_item_from_cart(request, pk, slug):
```

Reduces the quantity of an item in cart by 1, otherwise removes it entirely

#### Arguments

- `request` - request
- `pk` - item primary key
- `slug` - item slug

#### Returns

- `None` - redirects to previous page with quantity changed

## signup

[[find in source code]](..\..\core\views.py#L39)

```python
def signup(request):
```

## valid_form

[[find in source code]](..\..\core\views.py#L35)

```python
def valid_form(values):
```
