# Admin

> Auto-generated documentation for [core.admin](..\..\core\admin.py) module.

- [Group_project](..\README.md#group_project-index) / [Modules](..\MODULES.md#group_project-modules) / [Core](index.md#core) / Admin
    - [AddressAdmin](#addressadmin)
    - [OrderAdmin](#orderadmin)
    - [make_refund_accepted](#make_refund_accepted)

## AddressAdmin

[[find in source code]](..\..\core\admin.py#L53)

```python
class AddressAdmin(admin.ModelAdmin):
```

## OrderAdmin

[[find in source code]](..\..\core\admin.py#L22)

```python
class OrderAdmin(admin.ModelAdmin):
```

## make_refund_accepted

[[find in source code]](..\..\core\admin.py#L8)

```python
def make_refund_accepted(adminmodel, request, queryset):
```

Marks a Order as  having a refund granted. Used through the admin page under 'Orders'
    Args:
        adminmodel: not used
        request: not used
        queryset: Order
