# Forms

> Auto-generated documentation for [core.forms](..\..\core\forms.py) module.

- [Group_project](..\README.md#group_project-index) / [Modules](..\MODULES.md#group_project-modules) / [Core](index.md#core) / Forms
    - [CheckoutForm](#checkoutform)
    - [DiscountForm](#discountform)
    - [PaymentForm](#paymentform)
    - [RefundForm](#refundform)

## CheckoutForm

[[find in source code]](..\..\core\forms.py#L9)

```python
class CheckoutForm(forms.Form):
```

This handles the checkout data - addresses primarily. Payment data is handled separately by PaymentForm.

#### Arguments

- `forms.Form` - Passed from views.CheckoutView

#### Attributes

- `shipping_address` - first line of the address to ship to, required
- `shipping_country` - country the shipping address is located in, required
- `shipping_zip` - the zip code of the shipping address, required
- `same_billing_address` - Whether the shipping address is the same as the billing address, required, bool
- `billing_address` - first line of the address to bill the order to, required
- `billing_country` - country the billing address is located in, required
- `billing_zip` - the zip code of the billing address, required
- `set_default_shipping` - whether to save as default address, required, bool
- `use_default_shipping` - Use default address if available, required, bool
- `set_default_billing` - whether to save as default address, required, bool
- `use_default_billing` - Use default address if available, required, bool
- `payment_option` - what to pay with

## DiscountForm

[[find in source code]](..\..\core\forms.py#L48)

```python
class DiscountForm(forms.Form):
```

Allows the adding of a discount code to an order.

#### Arguments

- `forms.Form` - Passed from views.CheckoutView

#### Attributes

- `code` - discount code to use

## PaymentForm

[[find in source code]](..\..\core\forms.py#L85)

```python
class PaymentForm(forms.Form):
```

Used to process payments.

#### Arguments

- `forms.Form` - Passed from views.PaymentView

#### Attributes

- `card_num` - card number to pay with. Required. Length of 16.

## RefundForm

[[find in source code]](..\..\core\forms.py#L66)

```python
class RefundForm(forms.Form):
```

Used to grant refunds.

#### Arguments

- `forms.Form` - Passed from views.RequestRefundView

#### Attributes

- `ref_code` - reference code, generated separately, required
- `message` - reason for requesting refund. Optional
- `email` - email address. Optional
