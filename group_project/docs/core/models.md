# Models

> Auto-generated documentation for [core.models](..\..\core\models.py) module.

- [Group_project](..\README.md#group_project-index) / [Modules](..\MODULES.md#group_project-modules) / [Core](index.md#core) / Models
    - [Address](#address)
    - [Category](#category)
        - [Category().get_absolute_url](#categoryget_absolute_url)
        - [Category().save](#categorysave)
    - [DiscountCode](#discountcode)
    - [Item](#item)
        - [Item().get_absolute_url](#itemget_absolute_url)
        - [Item().get_add_to_cart_url](#itemget_add_to_cart_url)
        - [Item().get_remove_from_cart_url](#itemget_remove_from_cart_url)
        - [Item().save](#itemsave)
    - [Order](#order)
        - [Order().get_total](#orderget_total)
        - [Order().save](#ordersave)
    - [OrderItem](#orderitem)
        - [OrderItem().get_amount_saved](#orderitemget_amount_saved)
        - [OrderItem().get_final_price](#orderitemget_final_price)
        - [OrderItem().get_total_discount_item_price](#orderitemget_total_discount_item_price)
        - [OrderItem().get_total_item_price](#orderitemget_total_item_price)
    - [Payment](#payment)
    - [Refund](#refund)
    - [save_path](#save_path)

#### Attributes

- `ADDRESS_TYPES` - Create your models here.: `(('B', 'Billing'), ('S', 'Shipping'))`

## Address

[[find in source code]](..\..\core\models.py#L297)

```python
class Address(models.Model):
```

Addresses for each user. Each user can have multiple addresses, as they are set for each Order, unless default=True is used.

#### Arguments

- `models.Model` - --

#### Attributes

- `user` - foreign key user identifier, handled by the built in auth system
- `address` - first line of a address, max length=100
- `country` - country the address is located within. Select from list.
- `zip` - Zipcode of the address, max length 8
- `address_type` - multiple choice
- `default` - whether its the default address for the user, default False

## Category

[[find in source code]](..\..\core\models.py#L34)

```python
class Category(MPTTModel):
```

Allows us to use a tree structure to create a hierarchy for our alcohols, e.g. bombay sapphire is a gin,
which is a subcategory of spirit

#### Arguments

- `MPTTModel` - --

#### Attributes

- `name` - name of category, must be unique and =<50 characters
- `parent` - parent category, e.g. spirit. Can be null and blank (to allow for first level stuff, e.g. spirit)
- `slug` - path to use as url
- `name` - this allows us to use a tree structure to define what bit an item belongs to, e.g. gin, which is a subcategory
  of spirit: `models.CharField(max_length=50, unique=True)`

### Category().get_absolute_url

[[find in source code]](..\..\core\models.py#L67)

```python
def get_absolute_url():
```

Gets the absolute url for the category. Contains all items within that category.

### Category().save

[[find in source code]](..\..\core\models.py#L75)

```python
def save(*args, **kwargs):
```

Used to save the category's values. Useful when backdating categories.

#### Arguments

*args:
**kwargs:

## DiscountCode

[[find in source code]](..\..\core\models.py#L352)

```python
class DiscountCode(models.Model):
```

Discount codes. Each discount code can be used on multiple orders.

#### Arguments

- `models.Model` - --

#### Attributes

- `code` - discount code, max length 30
- `percentage` - how much of a discount the code gives

## Item

[[find in source code]](..\..\core\models.py#L87)

```python
class Item(models.Model):
```

Contains each item's data. An item = a product, e.g. bombay sapphire gin.

#### Arguments

- `models.Model` - --

#### Attributes

- `item_name` - name of alcohol, max length =<50 characters
- `item_description` - short description, max length =<250 characters
- `country_origin` - country of origin, max length =<50 characters
- `item_type` - type of alcohol, max length =<50 characters
- `category` - alcohol category
- `item_size` - size of alcohol, in centiliters.
- `price` - price of alcohol, max 6 digits, 2 decimal places
- `discount_price` - discounted price, blank if not discounted. Same rules as 'price'
- `image` - Image for the alcohol. Settings for image are stored in 'settings'
- `slug` - path to use as url
- `item_size` - in cl: `models.IntegerField()`

### Item().get_absolute_url

[[find in source code]](..\..\core\models.py#L145)

```python
def get_absolute_url():
```

Creates the url for the item's product page

### Item().get_add_to_cart_url

[[find in source code]](..\..\core\models.py#L123)

```python
def get_add_to_cart_url():
```

Creates the url to add the item to cart

### Item().get_remove_from_cart_url

[[find in source code]](..\..\core\models.py#L134)

```python
def get_remove_from_cart_url():
```

Creates the url to remove the item from cart

### Item().save

[[find in source code]](..\..\core\models.py#L157)

```python
def save(*args, **kwargs):
```

## Order

[[find in source code]](..\..\core\models.py#L220)

```python
class Order(models.Model):
```

The overarching Order. Each user can have multiple orders, and each order has multiple OrderItems.

#### Arguments

- `models.Model` - --

#### Attributes

- `user` - foreign key user identifier, handled by the built in auth system
- `items` - many to many field on OrderItem, one order can have any items.
- `start_date` - date the order was first initialised (first item added to cart), automatically added
- `ordered_date` - datetime, filled when ordered=True.
- `ordered` - bool, whether the order has been ordered and paid for.
- `shipping_address` - foreign key link to the shipping address held in 'Address'
- `billing_address` - foreign key link to the billing address held in 'Address'
- `payment` - foreign key link to the payment used for the order
- `discount_code` - foreign key link to the discount code (if there is one, null if not) of the order
- `being_delivered` - bool, wheter its being delivered. If True then delivery_date is automatically set.
- `delivery_date` - ordered_date + 3 days, date the delivery is planned for
- `received` - bool, whether the order has been delivered, default False
- `refund_requested` - bool, whether a refund has bene requested, default False
- `refund_granted` - bool, whether a refund has bene granted, default False
- `ref_code` - text reference code for order, automatically set, max length 20

### Order().get_total

[[find in source code]](..\..\core\models.py#L270)

```python
def get_total():
```

Calculates the total cost if a discount code is used

#### Returns

- `float` - total cost of order, minus discounted amount

### Order().save

[[find in source code]](..\..\core\models.py#L281)

```python
def save(*args, **kwargs):
```

Whenever the order is saved:
if the ordered file is True and ordered_date is blank, set ordered date as the current date and time
Then if delivery_date isn't set and ordered date is, set delivery date as ordered_date + 3 days

#### Arguments

*args:
**kwargs:

## OrderItem

[[find in source code]](..\..\core\models.py#L166)

```python
class OrderItem(models.Model):
```

Acts as the link between Item and Order. Each Item can have many OrderItems, and each Order is made up of many OrderItems

#### Arguments

- `models.Model` - --

#### Attributes

- `user` - foreign key user identifier, handled by the built in auth system
- `ordered` - whether order has been placed, bool
- `quantity` - quantity of the item ordered
- `item` - item obj

### OrderItem().get_amount_saved

[[find in source code]](..\..\core\models.py#L200)

```python
def get_amount_saved():
```

Get the amount saved by minusing the discounted price from the total price.

#### Returns

- `Decimal` - total price - discounted total price

### OrderItem().get_final_price

[[find in source code]](..\..\core\models.py#L208)

```python
def get_final_price():
```

Get the items price. If it's discounted, then that, otherwise the normal price.

#### Returns

Decimal:

### OrderItem().get_total_discount_item_price

[[find in source code]](..\..\core\models.py#L192)

```python
def get_total_discount_item_price():
```

Total price, taking into account discounted price and quantity ordered

#### Returns

- `Decimal` - quantity* discounted price

### OrderItem().get_total_item_price

[[find in source code]](..\..\core\models.py#L184)

```python
def get_total_item_price():
```

Total price, taking into account price and quantity ordered

#### Returns

- `Decimal` - quantity*price

## Payment

[[find in source code]](..\..\core\models.py#L328)

```python
class Payment(models.Model):
```

Payments for each user. Each user can have multiple Payments, as they are set for each Order.

#### Arguments

- `models.Model` - --

#### Attributes

- `user` - foreign key user identifier, handled by the built in auth system
- `payment_id` - automatically generated payment reference number
- `amount` - total charged amount
- `timestamp` - automatically added timestamp of the payment time
- `card_number` - debit card number, max length 16

## Refund

[[find in source code]](..\..\core\models.py#L370)

```python
class Refund(models.Model):
```

Addresses for each user. Each user can have multiple addresses, as they are set for each Order, unless default=True is used.

#### Arguments

- `models.Model` - --

#### Attributes

- `order` - foreign key on Order, links the refund to an order
- `reason` - why they want the refund
- `accepted` - bool, whether the refund was granted
- `email` - email address of the user

## save_path

[[find in source code]](..\..\core\models.py#L20)

```python
def save_path(item, filename):
```

Allows us to access item_type, as we cannot do it in the class itself. Creates a filepath to save the image to.

#### Arguments

- `item` - item class obj
- `filename` - not needed

#### Returns

- `str` - filepath
