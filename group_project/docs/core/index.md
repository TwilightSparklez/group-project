# Core

> Auto-generated documentation for [core](..\..\core\__init__.py) module.

- [Group_project](..\README.md#group_project-index) / [Modules](..\MODULES.md#group_project-modules) / Core
    - Modules
        - [Admin](admin.md#admin)
        - [Forms](forms.md#forms)
        - [Models](models.md#models)
        - [Storage](storage.md#storage)
        - [Views](views.md#views)
