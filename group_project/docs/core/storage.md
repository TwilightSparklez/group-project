# Storage

> Auto-generated documentation for [core.storage](..\..\core\storage.py) module.

- [Group_project](..\README.md#group_project-index) / [Modules](..\MODULES.md#group_project-modules) / [Core](index.md#core) / Storage
    - [OverwriteStorage](#overwritestorage)
        - [OverwriteStorage().get_available_name](#overwritestorageget_available_name)

## OverwriteStorage

[[find in source code]](..\..\core\storage.py#L4)

```python
class OverwriteStorage(FileSystemStorage):
```

### OverwriteStorage().get_available_name

[[find in source code]](..\..\core\storage.py#L6)

```python
def get_available_name(name, max_length=None):
```

Deletes a file and returns the name, so it can be reused.

#### Arguments

- `name` - name of file to delete
- `max_length` - Max length, not used.

#### Returns

- `name` - name of file
