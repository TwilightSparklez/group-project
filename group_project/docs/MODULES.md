# Group_project Modules

> Auto-generated documentation modules index.

Full list of [Group_project](README.md#group_project-index) project modules.

- [Group_project Index](README.md#group_project-index)
- [Core](core\index.md#core)
    - [Admin](core\admin.md#admin)
    - [Forms](core\forms.md#forms)
    - [Models](core\models.md#models)
    - [Storage](core\storage.md#storage)
    - [Views](core\views.md#views)
